import React from 'react';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  Text,
  View,
} from 'react-native';
import {List} from 'react-native-paper';
import httpService from '../services/http.service';

interface SettingsProps {}

interface SettingsState extends GetPassengerResponse {
  page: number;
  size: number;
  refreshing: boolean;
  loading: boolean;
}

export interface GetPassengerResponse {
  totalPassengers: number;
  totalPages: number;
  data: Passenger[];
}

interface Airline {
  id: number;
  name: string;
  country: string;
  logo: string;
  slogan: string;
  head_quaters: string;
  website: string;
  established: string;
}

interface Passenger {
  _id: string;
  name: string;
  trips: number;
  airline: Airline[];
  __v: number;
}

interface ListItemProps {
  item: Passenger;
  index: number;
}

class ListItem extends React.Component<ListItemProps> {
  shouldComponentUpdate() {
    return false;
  }
  render() {
    return <List.Item title={this.props.item.name} />;
  }
}

export default class Settings extends React.PureComponent<
  SettingsProps,
  SettingsState
> {
  constructor(props: SettingsProps) {
    super(props);
    this.state = {
      data: [],
      totalPages: 0,
      totalPassengers: 0,
      page: 1,
      size: 15,
      refreshing: false,
      loading: false,
    };
  }

  async componentDidMount() {
    await this.getData(this.state.page, this.state.size);
  }

  async getData(page: number, size: number) {
    this.setState({loading: true});
    const getPassengerResponse = await httpService.axiosIntance.get<GetPassengerResponse>(
      `/passenger`,
      {
        params: {
          page,
          size,
        },
      },
    );
    console.log('getPassengerResponse', getPassengerResponse.data.totalPages);
    const newData = [...this.state.data, ...getPassengerResponse.data.data];
    this.setState({
      data: newData,
      page,
      size,
      loading: false,
    });
  }

  refreshData = async () => {
    this.setState({refreshing: true});
    await this.getData(1, 15);
    this.setState({refreshing: false});
  };

  getNextPage = async () => {
    return await this.getData(this.state.page + 1, this.state.size);
  };

  renderItem = ({item, index}: {item: Passenger; index: number}) => {
    return <ListItem item={item} index={index} />;
  };

  render() {
    return (
      <>
        <FlatList
          data={this.state.data}
          initialNumToRender={this.state.size}
          keyExtractor={(item, index) =>
            item._id.toString() + item.name + index.toString()
          }
          renderItem={this.renderItem}
          refreshing={this.state.refreshing}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.refreshData}
            />
          }
          onEndReached={this.getNextPage}
          onEndReachedThreshold={1}
          getItemLayout={(data, index) => ({length: 15, offset: 0, index})}
          removeClippedSubviews={true}
          maxToRenderPerBatch={15}
        />
        {this.state.loading && <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'grey',
            opacity: 0.5
          }}>
          <ActivityIndicator
            animating={true}
            size="large"
            color="red"
          />
        </View>}
      </>
    );
  }
}
