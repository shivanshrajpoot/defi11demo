import React from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Home from '../screens/Home';
import Settings from '../screens/Settings';

const Tab = createMaterialBottomTabNavigator();

export default function MyTabs() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={Home} options={{
        tabBarIcon: 'home'
      }} />
      <Tab.Screen name="List" component={Settings} options={{
        tabBarIcon: 'account-settings'
      }} />
    </Tab.Navigator>
  );
}