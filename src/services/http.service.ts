import axios, { AxiosInstance } from 'axios';
const API_VERSION = 'v1';
const API_BASE_URL = `https://api.instantwebtools.net/${API_VERSION}`;
class HttpService {
    readonly axiosIntance: AxiosInstance;
    constructor(baseURL: string){
        this.axiosIntance = axios.create({
            baseURL,
            timeout: 10000,
        });
    }

}

export default new HttpService(API_BASE_URL);